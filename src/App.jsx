import { useEffect, useState } from 'react'

// Hooks
import useFetch from './hooks/useFetch'

// React Router
import { Routes, Route, useNavigate } from 'react-router-dom'

// Components
import UserForm from './Components/UserForm'
import Quiz from './Components/Quiz'

// Styles
import GlobalStyle from './GlobalStyle'

const USERS_KEY = 'USERS'
const ACTIVE_USER_KEY = 'ACTIVE_USER'
const API_URL =
	'https://opentdb.com/api.php?amount=5&category=9&difficulty=easy&type=multiple'

const App = () => {
	const navigate = useNavigate()

	const [pageState, setPageState] = useState('login')
	const [answers, setAnwers] = useState([])
	const [score, setScore] = useState(0)
	const { questions } = useFetch(API_URL, pageState)
	const [users, setUsers] = useState(
		() => JSON.parse(localStorage.getItem(USERS_KEY)) || []
	)
	const [activeUser, setActiveUser] = useState(
		() => JSON.parse(localStorage.getItem(ACTIVE_USER_KEY)) || null
	)

	const shuffle = arr => {
		for (let i = arr.length - 1; i > 0; i--) {
			let j = Math.floor(Math.random() * (i + 1))
			;[arr[i], arr[j]] = [arr[j], arr[i]]
		}

		return arr
	}

	useEffect(() => {
		localStorage.setItem(USERS_KEY, JSON.stringify(users))
	}, [users])

	useEffect(() => {
		localStorage.setItem(ACTIVE_USER_KEY, JSON.stringify(activeUser))
	}, [activeUser])

	useEffect(() => {
		const answersData = questions.map(question => {
			const shuffledAnswer = shuffle([
				question.correct_answer,
				...question.incorrect_answers
			])

			return shuffledAnswer
		})

		setAnwers(answersData)
	}, [questions])

	useEffect(() => {
		if (activeUser === null) {
			navigate('/')
		} else {
			navigate('/quiz')
		}
	}, [activeUser])

	return (
		<>
			<Routes>
				<Route
					index
					element={
						<UserForm
							pageState={pageState}
							setPageState={setPageState}
							users={users}
							setUsers={setUsers}
							setActiveUser={setActiveUser}
						/>
					}
				/>
				<Route
					path='/quiz'
					element={
						<Quiz
							pageState={pageState}
							setPageState={setPageState}
							activeUser={activeUser}
							setActiveUser={setActiveUser}
							questions={questions}
							answers={answers}
							score={score}
							setScore={setScore}
						/>
					}
				/>
			</Routes>
			<GlobalStyle />
		</>
	)
}

export default App
