import { useEffect, useState } from 'react'

const useFetch = (API_URL, pageState) => {
	const [questions, setData] = useState([])

	useEffect(() => {
		const fetchData = async () => {
			try {
				const response = await fetch(API_URL)
				const data = await response?.json()
				const questionsData = data?.results

				setData(questionsData)
			} catch (err) {
				console.error(err)
			}
		}

		fetchData()
	}, [API_URL, pageState])

	return { questions }
}

export default useFetch
