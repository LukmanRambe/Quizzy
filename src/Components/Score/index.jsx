// Components
import { Copywrite } from '../UserForm/UserForm.styles'

// Styles
import {
	ScoreCard,
	ScoreTitle,
	Username,
	UserScore,
	RestartButton
} from './Score.styles'

const Score = ({
	setPageState,
	setCurrentQuestion,
	activeUser,
	score,
	questionsLength,
	questionsAnswered,
	setSeconds
}) => {
	const restartQuiz = () => {
		setSeconds(60)
		setCurrentQuestion(0)
		setPageState('home')
	}

	return (
		<>
			<ScoreCard>
				<ScoreTitle>Score</ScoreTitle>
				<Copywrite>
					Nice work <Username>{activeUser.username}</Username>, you have
					answered{' '}
					<span className='questions-answered'>{questionsAnswered}</span>{' '}
					questions!
					<br /> Here's your score
				</Copywrite>
				<UserScore>
					{score} / {questionsLength}
				</UserScore>
				<RestartButton onClick={restartQuiz}>Restart Quiz</RestartButton>
			</ScoreCard>
		</>
	)
}

export default Score
