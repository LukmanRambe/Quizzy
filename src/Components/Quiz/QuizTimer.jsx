import { useEffect } from 'react'
import { Copywrite } from '../UserForm/UserForm.styles'
import { StartButton } from './Quiz.styles'

// Styles
import { Timer } from './Quiz.styles'

const QuizTimer = ({
	questions,
	pageState,
	setPageState,
	setScore,
	setOptionChosen,
	isTimerActive,
	setIsTimerActive,
	seconds,
	setSeconds
}) => {
	const formatSeconds = secs => {
		const pad = n => (n < 10 ? `0${n}` : n)

		const h = Math.floor(secs / 3600)
		const m = Math.floor(secs / 60) - h * 60
		const s = Math.floor(secs - h * 3600 - m * 60)

		return `${pad(m)} : ${pad(s)}`
	}

	useEffect(() => {
		let interval = null
		if (isTimerActive) {
			interval = setInterval(() => {
				setSeconds(prevSeconds => prevSeconds - 1)
			}, 1000)
		}

		return () => clearInterval(interval)
	}, [isTimerActive, setSeconds])

	useEffect(() => {
		if (seconds <= 0) {
			setScore(prevState => prevState)
			setIsTimerActive(false)
			setPageState('finished')
		}
	}, [seconds])

	const handleStart = () => {
		setOptionChosen(null)
		setSeconds(60)
		setIsTimerActive(true)
		setPageState('quiz')
	}

	return (
		<>
			{pageState === 'quiz' ? (
				<Timer>
					Time Remaining <br /> <span></span>
					<span>{formatSeconds(seconds)}</span>
				</Timer>
			) : (
				pageState !== 'finished' && (
					<>
						<Copywrite>
							There are <span>{questions.length}</span> questions and{' '}
							<span>{formatSeconds(seconds)}</span> minute to answers it all
						</Copywrite>
						<StartButton onClick={handleStart}>Start Quiz</StartButton>
					</>
				)
			)}
		</>
	)
}

export default QuizTimer
