// Hooks
import { useState } from 'react'

// Library
import { AES, enc } from 'crypto-js'

// React Router
import { useNavigate } from 'react-router-dom'

// Styles
import {
	Container,
	Content,
	Title,
	Card,
	CardTitle,
	Form,
	Label,
	Input,
	Button,
	Copywrite
} from './UserForm.styles'

const Home = ({ pageState, setPageState, users, setUsers, setActiveUser }) => {
	const navigate = useNavigate()

	const [userInputs, setUserInputs] = useState({
		username: '',
		password: '',
		confirmPassword: ''
	})
	const { username, password, confirmPassword } = userInputs

	const handleChange = e => {
		setUserInputs(prevState => {
			return {
				...prevState,
				[e.target.name]: e.target.value
			}
		})
	}

	const handleClick = () => {
		setPageState(prevState => (prevState === 'login' ? 'signUp' : 'login'))
	}

	const handleSubmit = e => {
		e.preventDefault()

		const userObj = {
			id: new Date().getTime().toString(),
			username,
			password: AES.encrypt(password, 'passPassphrases').toString()
		}

		if (pageState === 'login') {
			if (
				username.toLowerCase().trim() !== '' &&
				password.toLowerCase().trim() !== ''
			) {
				const userData = users.find(user => user.username === username)
				const decryptedPassword = AES.decrypt(
					userData.password,
					'passPassphrases'
				)

				if (userData) {
					if (decryptedPassword.toString(enc.Utf8) !== password) {
						setUserInputs({ username, password: '', confirmPassword: '' })
						alert('Wrong Password')
					} else {
						setPageState('home')
						setActiveUser({
							id: userData.id,
							username: userData.username
						})
						setUserInputs({ username: '', password: '', confirmPassword: '' })
						alert('Login success!')
						navigate('/quiz')
					}
				}
			} else {
				alert('Fill in the all the fields')
			}
		}

		if (pageState === 'signUp') {
			const userData = users.find(user => user.username === username)

			if (userData) {
				alert('Username already taken!')
			} else {
				if (
					username.toLowerCase().trim() !== '' &&
					password.toLowerCase().trim() !== ''
				) {
					if (password.length < 8) {
						alert('Password must be at least 8 characters')
					} else {
						if (
							password.toLowerCase().trim() !==
							confirmPassword.toLowerCase().trim()
						) {
							alert('Password must be match!')
						} else {
							setUserInputs({ username: '', password: '', confirmPassword: '' })
							setUsers(prevState => {
								return [...prevState, userObj]
							})
							alert('Sign Up Success!')
							setPageState('login')
						}
					}
				} else {
					alert('Fill in the all the fields')
				}
			}
		}
	}

	return (
		<>
			<Container>
				<Content>
					<Title>Quizzy</Title>
					<Copywrite>
						Test your knowledge with trivial questions <br /> Have fun and enjoy
					</Copywrite>
					<Card>
						<CardTitle>{pageState === 'login' ? 'Login' : 'Sign Up'}</CardTitle>
						<Form onSubmit={handleSubmit}>
							<div>
								<Label htmlFor='username'>Username</Label>
								<Input
									type='text'
									id='username'
									name='username'
									value={username}
									aria-label='username'
									onChange={handleChange}
								/>
							</div>

							<div>
								<Label htmlFor='password'>Password</Label>
								<Input
									type='password'
									id='password'
									name='password'
									value={password}
									aria-label='password'
									onChange={handleChange}
								/>
							</div>

							{pageState === 'signUp' && (
								<div>
									<Label htmlFor='confirmPassword'>Confirm Password</Label>
									<Input
										type='password'
										id='confirmPassword'
										name='confirmPassword'
										value={confirmPassword}
										aria-label='confirmPassword'
										onChange={handleChange}
									/>
								</div>
							)}

							<div className='btn-group'>
								<Button type='submit'>
									{pageState === 'login' ? 'Login' : 'Sign Up'}
								</Button>
								<p>
									{pageState === 'login'
										? 'New to Quizzy?'
										: 'Already have an account?'}
									<span className='link' onClick={handleClick}>
										{pageState === 'login' ? ' Sign Up' : ' Login'}
									</span>
								</p>
							</div>
						</Form>
					</Card>
				</Content>
			</Container>
		</>
	)
}

export default Home
